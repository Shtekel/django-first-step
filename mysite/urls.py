"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from polls.viewsets.car_viewset import CarViewSet
from polls.viewsets.company_viewset import CompanyViewSet
from polls.viewsets.prices_viewset import PricesViewSet
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

# admin.autodiscover()
# router = ExtendedDefaultRouter()
# (
#     router.register('cars', CarViewSet, base_name='cars'),
#     router.register('companys', CompanyViewSet, base_name='companys'),
#     router.register('prices', PricesViewSet, base_name='prices')
# )

# urlpatterns = patterns('',
#                        url('', include(router.urls))
#                        )


urlpatterns = [
    url(r'^polls/', include('polls.urls')),
    # url(r'^/api/', include('rest_framework.urls')),
    url(r'^admin/', admin.site.urls),
]
