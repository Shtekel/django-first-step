var LoginModalController = {
    tabsElementName: ".logmod__tabs li",
    tabElementName: ".logmod__tab",
    inputElementsName: ".logmod__form .input",
    hidePasswordName: ".hide-password",
    
    inputElements: null,
    tabsElement: null,
    tabElement: null,
    hidePassword: null,
    
    activeTab: null,
    tabSelection: 0, // 0 - first, 1 - second
    
    findElements: function () {
        var base = this;
        
        base.tabsElement = $(base.tabsElementName);
        base.tabElement = $(base.tabElementName);
        base.inputElements = $(base.inputElementsName);
        base.hidePassword = $(base.hidePasswordName);
        
        return base;
    },
    
    setState: function (state) {
    	var base = this,
            elem = null;
        
        if (!state) {
            state = 0;
        }
        
        if (base.tabsElement) {
        	elem = $(base.tabsElement[state]);
            elem.addClass("current");
            $("." + elem.attr("data-tabtar")).addClass("show");
        }
  
        return base;
    },
    
    getActiveTab: function () {
        var base = this;
        
        base.tabsElement.each(function (i, el) {
           if ($(el).hasClass("current")) {
               base.activeTab = $(el);
           }
        });
        
        return base;
    },
   
    addClickEvents: function () {
    	var base = this;
        
        base.hidePassword.on("click", function (e) {
            var $this = $(this),
                $pwInput = $this.prev("input");
            
            if ($pwInput.attr("type") == "password") {
                $pwInput.attr("type", "text");
                $this.text("Hide");
            } else {
                $pwInput.attr("type", "password");
                $this.text("Show");
            }
        });
 
        base.tabsElement.on("click", function (e) {
            var targetTab = $(this).attr("data-tabtar");
            
            e.preventDefault();
            base.activeTab.removeClass("current");
            base.activeTab = $(this);
            base.activeTab.addClass("current");
            
            base.tabElement.each(function (i, el) {
                el = $(el);
                el.removeClass("show");
                if (el.hasClass(targetTab)) {
                    el.addClass("show");
                }
            });
        });
        
        base.inputElements.find("label").on("click", function (e) {
           var $this = $(this),
               $input = $this.next("input");
            
            $input.focus();
        });
        
        return base;
    },
    
    initialize: function () {
        var base = this;
        
        base.findElements().setState().getActiveTab().addClickEvents();
    }
};

$(document).ready(function() {
    LoginModalController.initialize();
});

//####################priceList_for_one_car/carname

$(function() {
$('#cartypebtn').click(function(e) {
        e.preventDefault();
        var carname =  document.getElementById("cartext").value
        var urlstr =  '/polls/priceList_for_one_car/carname=' + carname
        $.ajax({
            url: urlstr,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {

                var $div = $('#carTypeForm');
                if(response.length !== 0 ) {
                    $div.contents().remove();
                    $div.addClass('table-table-inverse');

                    var trHTML = '';
                    trHTML +=
                        '<table>' +
                        '<thead>' +

                        '<tr><th>' + "סוג הרכב" +
                        '</th><th>' + "מחיר" + '</th>' +
                        '<th>' + "שם חברה" + '</th>' +
                        '</tr>' +
                        '</thead>';

                    trHTML += '<tbody>';
                    for (var i = 0; i < response.length; i++) {
                        trHTML +=

                            '<tr><td>' + carname +
                            '</td><td>' + response[i].fields.price_at_company +
                            '</td><td>' + response[i].fields.company_connection +
                            '</td></tr>'

                    }
                    ;
                    trHTML += '</tbody>';
                    trHTML += '</table>'
                    $div.append(trHTML);
                    var blob=new Blob([response]);
                    var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="Dossier_"+new Date()+".xlsx";
                    link.click();
                }
                else
                {
                    $div = $('#carhelp');
                    $div.html('<p>' +  'לא קיימת חברה כזאת'+' ' + carname+ '</p>');

                }



            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});



//####################/polls/all_the_cars_from_the_active_company



$(function() {
$('#companysendbtn').click(function(e) {
        e.preventDefault();
        var urlstr =  '/polls/all_the_cars_from_the_active_company';
        $.ajax({
            url: urlstr,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                var $div = $('#companyTypeForm');
                if(response != undefined) {
                    $div.contents().remove();
                    $div.addClass('table-table-inverse');

                    var trHTML = '';
                    trHTML +=
                        '<table>' +
                        '<thead>' +

                        '<tr><th>' + "סוג הרכב" +
                        '</th><th>' + "מחיר" + '</th></tr>' +
                        '</thead>';

                    trHTML += '<tbody>';
                    for (var i = 0; i < response.length; i++) {
                        trHTML +=

                            '<tr><td>' + response[i].fields.car_name +
                            '</td><td>' + response[i].fields.car_price +
                            '</td></tr>'

                    }
                    ;
                    trHTML += '</tbody>';
                    trHTML += '</table>'
                    $div.append(trHTML);
                }
                else
                {
                    alert('לא קיימת מכונית כזאת');
                }


            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});







//Companies_that_active_and_their_name_contains_hebrew_yod

$(function() {
$('#companyyodtbtn').click(function(e) {
        e.preventDefault();
        var urlstr =  '/polls/Companies_that_active_and_their_name_contains_hebrew_yod';
        $.ajax({
            url: urlstr,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                var $div = $('#companywithyodForm');
                if(response != undefined) {
                    $div.contents().remove();
                    $div.addClass('table-table-inverse');

                    var trHTML = '';
                    trHTML +=
                        '<table>' +
                        '<thead>' +
                        '<tr><th>' + "שם החברה" +
                        '</th>' +
                        '<th>' + "עיר" +
                        '</th>' +
                        '<th>' + "מספר הטלפון" +
                        '</th>' +
                        '</tr>' +
                        '</thead>';

                    trHTML += '<tbody>';
                    for (var i = 0; i < response.length; i++) {
                        trHTML +=

                            '<tr><td>' + response[i].fields.company_name +
                            '</td><td>' + response[i].fields.company_city +
                            '</td><td>' + response[i].fields.company_phone_number +
                            '</td></tr>'

                    }
                    ;
                    trHTML += '</tbody>';
                    trHTML += '</table>'
                    $div.append(trHTML);
                }
                else
                {
                    alert('לא קיימת מכונית כזאת');
                }


            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});

//priceList_for_company


$(function() {
$('#companypricelistbtn').click(function(e) {
        e.preventDefault();
        var companyname =  document.getElementById("companytext").value;
        var urlstr =  '/polls/priceList_for_company/companyname=' + companyname;
        $.ajax({
            url: urlstr,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                var $div = $('#companypriceTypeForm');
                if(response != undefined) {
                    $div.contents().remove();
                    $div.addClass('table-table-inverse');

                    var trHTML = '';
                    trHTML +=
                        '<table>' +
                        '<thead>' +

                        '<tr><th>' + "מחיר" +
                        '</th>' +
                        '<th>' + "סוג מכונית" + '</th>' +
                        '<th>' + "שם חברה" + '</th>' +
                        '</tr>' +
                        '</thead>';

                    trHTML += '<tbody>';
                    for (var i = 0; i < response.length; i++) {
                        trHTML +=

                            '<tr><td>' + response[i].fields.price_at_company +
                            '</td><td>' + response[i].fields.car_connection +
                            '</td><td>' + response[i].fields.company_connection +
                            '</td></tr>'

                    }
                    ;
                    trHTML += '</tbody>';
                    trHTML += '</table>'
                    $div.append(trHTML);
                }
                else
                {
                    $div.html('<p>' +  'לא קיימת חברה כזאת'+' ' + carname+ '</p>');
                }


            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});

//min price

$(function() {
$('#companyminpricetbtn').click(function(e) {
        e.preventDefault();
        var urlstr =  '/polls/min_price_list_in_every_company';
        $.ajax({
            url: urlstr,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                var $div = $('#companyminpriceForm');
                if(response != undefined) {
                    $div.contents().remove();
                    $div.addClass('table-table-inverse');

                    var trHTML = '';
                    trHTML +=
                        '<table>' +
                        '<thead>' +

                        '<tr><th>' + "מחיר" +
                        '</th>' +
                        '<th>' + "סוג מכונית" + '</th>' +
                        '<th>' + "שם חברה" + '</th>' +
                        '</tr>' +
                        '</thead>';

                    trHTML += '<tbody>';
                    for (var i = 0; i < response.length; i++) {
                        trHTML +=

                            '<tr><td>' + response[i].fields.price_at_company +
                            '</td><td>' + response[i].fields.car_connection +
                            '</td><td>' + response[i].fields.company_connection +
                            '</td></tr>'

                    }
                    ;
                    trHTML += '</tbody>';
                    trHTML += '</table>'
                    $div.append(trHTML);
                }
                else
                {
                    $div.html('<p>' +  'אין מידע ב-DB'+ '</p>');
                }


            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});



