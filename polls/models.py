# coding=utf-8
from __future__ import unicode_literals
from django.db import models
import datetime
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from query_set import CarQuerySetManager,CompanyQuerySetManager,PricesQuerySetManager




class Car(models.Model):
    Company = models.ManyToManyField('Company' , through='Prices')
    car_name = models.CharField(max_length=200)
    car_price = models.FloatField()
    # car_timestamp = models.DateTimeField('date published',default=datetime.datetime.g)
    objects = CarQuerySetManager.as_manager()

    def natural_key(self):
        return (self.car_name)

    def __unicode__(self):
       return self.car_name

    def __str__(self):
        return self.car_name

class Company(models.Model):



    choises_city = ((u'תל אביב',u'תל אביב')
                    ,(u'יהוד',u'יהוד'),
                    (u'חיפה',u'חיפה'))

    company_name = models.CharField(max_length=200)
    company_city = models.CharField(max_length=200,choices=choises_city,default=u'ירושלים')
    company_phone_number = models.CharField(max_length=200)
    company_is_active = models.BooleanField(default=0)
    objects = CompanyQuerySetManager.as_manager()

    def natural_key(self):
        return (self.company_name)

    def __unicode__(self):
       return self.company_name

    def __str__(self):
        return self.company_name


class Prices(models.Model):
    price_at_company = models.FloatField()
    car_connection = models.ForeignKey(Car, on_delete=models.CASCADE)
    company_connection = models.ForeignKey(Company, on_delete=models.CASCADE)
    objects = PricesQuerySetManager.as_manager()



    def __unicode__(self):
        return "{0}:{1} ".format(self.car_connection, self.company_connection)

    def __str__(self):
        return "{0}:{1} ".format(self.car_connection, self.company_connection)



