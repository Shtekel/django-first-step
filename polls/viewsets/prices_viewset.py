from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins  import NestedViewSetMixin
from polls.models import Prices
from polls.serializers.price_serializer import PricesSerialzier


class PricesViewSet(NestedViewSetMixin, ModelViewSet):
      
      serializer_class = PricesSerialzier
      queryset = Prices.objects.all()