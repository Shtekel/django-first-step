from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from polls.models import Car
from polls.serializers.car_serializer import CarSerialzier


class CarViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = CarSerialzier
    queryset = Car.objects.all()
