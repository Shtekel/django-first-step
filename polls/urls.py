from . import views
from polls.viewsets.car_viewset import CarViewSet
from polls.viewsets.company_viewset import CompanyViewSet
from polls.viewsets.prices_viewset import PricesViewSet
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

admin.autodiscover()
router = ExtendedDefaultRouter()
(
    router.register('cars', CarViewSet, base_name='cars'),
    router.register('companys', CompanyViewSet, base_name='companys'),
    router.register('prices', PricesViewSet, base_name='prices')
)


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^priceList_for_company/companyname=(?P<company_name>.+)/$', views.priceList_for_company, name='priceList_for_company'),
    url(r'^priceList_for_one_car/carname=(?P<car_name>.+)/$', views.priceList_for_one_car, name='priceList_for_one_car'),
    url(r'^all_the_cars_from_the_active_company/$', views.all_the_cars_from_the_active_company, name='all_the_cars_from_the_active_company'),
    url(r'^Companies_that_active_and_their_name_contains_hebrew_yod/$', views.Companies_that_active_and_their_name_contains_hebrew_yod, name='Companies_that_active_and_their_name_contains_hebrew_yod'),
    url(r'^min_price_list_in_every_company/$', views.min_price_list_in_every_company, name='min_price_list_in_every_company'),
    url('', include(router.urls)),
]
