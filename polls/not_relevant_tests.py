# coding=utf-8
from django.test import TestCase
from . import views
from django.test import Client
from . models import Company,Car,Prices
import os.path
import pandas as pd

class Django_tutrial_test(TestCase):

    def setUp(self):
        self.client = Client()

    def test_priceList_for_one_car(self):
        respone = self.client.get('/polls/priceList_for_one_car/carname=סוזוקי/')
        self.assertEqual(respone.status_code, 200)


    def test_priceList_for_company_query(self):
        test_company_name = ur'סיילסטק'
        query = Prices.objects.filter(company_connection__company_name=test_company_name)

    def test_priceList_for_company_get(self):
        respone = self.client.get('/polls/priceList_for_company/companyname=סיילסטק/')
        self.assertEqual(respone.status_code, 200)


    def test_companies_that_active_and_their_name_contains_hebrew_yod_query_count(self):
        yod = ur'י'
        query_expected_count = 32
        query = Company.objects.filter(company_name__contains=yod)
        print query.__str__()
        if query.count() !=  query_expected_count:
            pass

    def test_companies_that_active_and_their_name_contains_hebrew_yod_get(self):
        respone = self.client.get('/polls/Companies_that_active_and_their_name_contains_hebrew_yod/')
        self.assertEqual(respone.status_code, 200)

    def export_to_xl_test(self):
        test_file_name = 'test.xlsx'
        test_df = pd.DataFrame(index=range(0,4),columns=['A','id'])
        views.export_to_xl(test_df,test_file_name)
        if(os.path.isfile(test_file_name)==False):
            self.fail()




