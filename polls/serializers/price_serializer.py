from rest_framework import serializers
from polls.models import Prices


class PricesSerialzier(serializers.ModelSerializer):
    class Meta:
        model = Prices
        fields = ('car_connection','company_connection','price_at_company')