from django.contrib import admin

from .models import Car,Company,Prices

admin.site.register(Car)
admin.site.register(Company)
admin.site.register(Prices)