from django.db import models
from django.db.models import Min,F



class CompanyQuerySetManager(models.QuerySet):

    def active_companys(self):
        return self.filter(company_is_active=True)

    def companys_that_contains_hebrew_letter(self, letter):
        return self.filter(company_name__contains=letter)


class CarQuerySetManager(models.QuerySet):

    company_query_set_manager = CompanyQuerySetManager()

    def car_from_active_companys(self):
        return self.filter(Company__company_is_active=True)



class PricesQuerySetManager(models.QuerySet):

    def prices_list_by_company(self,company_name):
        return self.filter(company_connection__company_name=company_name)

    def prices_list_by_one_car(self,car_name):
        return self.filter(car_connection__car_name=car_name)

    def min_price_for_each_company(self):

        return self.annotate(minimum_price = Min('company_connection__prices__price_at_company')).filter(price_at_company=F('minimum_price')).only('car_connection','company_connection')

        # return self.only('company_connection').annotate(min_price=Min('price_at_company'))
        # return self.raw('SELECT MIN(price_at_company) FROM Prices ')