# coding=utf-8
from django.test import TestCase
from polls import views
from django.test import Client
from polls.models import Company,Car,Prices



class Test_all_the_cars_from_the_active_company(TestCase):

    def setUp(self):
        self.client = Client()
        self.car = Car(car_price='1',car_name='lada')
        self.car.save()
        self.company = Company(company_city='Tel aviv',company_phone_number='054',company_name='HP')
        self.company.save()
        self.price = Prices(car_connection=self.car,company_connection=self.company,price_at_company='120')
        self.price.save()
        self.car2 = Car(car_price='55',car_name='lada')
        self.car2.save()
        self.company2 = Company(company_city='Tel aviv',company_phone_number='054',company_name='Herouko')
        self.company2.save()
        self.price2 = Prices(car_connection=self.car2,company_connection=self.company2,price_at_company='450')
        self.price2.save()

    def test_200_respone(self):
        respone = self.client.get('/polls/min_price_list_in_every_company/')
        self.assertEqual(respone.status_code, 200)

    def test_serializer_respone(self):
        respone = self.client.get('/polls/min_price_list_in_every_company')
        serializers_respone = views.min_price_list_in_every_company(respone)
        self.assertContains(serializers_respone, '120')
        self.assertContains(serializers_respone, 'HP')
        self.assertContains(serializers_respone, 'lada')


