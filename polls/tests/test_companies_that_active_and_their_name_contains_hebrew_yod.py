# coding=utf-8
from django.test import TestCase
from polls import views
from django.test import Client
from polls.models import Company,Car,Prices


class Test_companies_that_active_and_their_name_contains_hebrew_yod(TestCase):

    def setUp(self):
        self.client = Client()
        self.car = Car(car_price='1',car_name='lada')
        self.car.save()
        self.company = Company(company_city='Tel aviv',company_phone_number='054',company_name=ur'יילו')
        self.company.save()
        self.price = Prices(car_connection=self.car,company_connection=self.company,price_at_company='120')
        self.price.save()

    def test_200_respone(self):
        respone = self.client.get('/polls/Companies_that_active_and_their_name_contains_hebrew_yod/')
        self.assertEqual(respone.status_code, 200)

    def test_serializer_respone(self):
        respone = self.client.get('/polls/Companies_that_active_and_their_name_contains_hebrew_yod/')
        serializers_respone = views.min_price_list_in_every_company(respone)
        print serializers_respone
        self.assertContains(serializers_respone, '120')
        self.assertContains(serializers_respone, 'HP')
        self.assertContains(serializers_respone, 'lada')
