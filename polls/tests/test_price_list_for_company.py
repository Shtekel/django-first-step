# coding=utf-8
from django.test import TestCase
from polls import views
from django.test import Client
from polls.models import Company,Car,Prices
import json
from pprint import pprint


class Test_min_price_list_for_one_car(TestCase):

    def setUp(self):
        self.client = Client()
        self.car = Car(car_price='1',car_name='lada')
        self.car.save()
        self.company = Company(company_city='Tel aviv',company_phone_number='054',company_name='HP')
        self.company.save()
        self.price = Prices(car_connection=self.car,company_connection=self.company,price_at_company='120')
        self.price.save()

    def test_200_respone(self):
        respone = self.client.get('/polls/priceList_for_company/companyname={0}/'.format(self.company.company_name))
        self.assertEqual(respone.status_code, 200)

    def test_serializer_respone(self):
        respone = self.client.get('/polls/priceList_for_company/companyname={0}/'.format(self.company.company_name))
        serializers_respone = views.priceList_for_company(respone,self.company.company_name)
        self.assertContains(serializers_respone, '120')
        self.assertContains(serializers_respone, 'HP')
        self.assertContains(serializers_respone, 'lada')


