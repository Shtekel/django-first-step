# coding=utf-8
from django.test import TestCase
from polls import views
from django.test import Client
from polls.models import Company,Car,Prices



class Test_all_the_cars_from_the_active_company(TestCase):

    def setUp(self):
        self.client = Client()
        self.car = Car(car_price='1',car_name='lada')
        self.car.save()
        self.company = Company(company_city='Tel aviv',company_phone_number='054',company_name='HP')
        self.company.save()
        self.price = Prices(car_connection=self.car,company_connection=self.company,price_at_company='120')
        self.price.save()

    def test_200_respone(self):
        respone = self.client.get('/polls/all_the_cars_from_the_active_company/'.format(self.car.car_name))
        self.assertEqual(respone.status_code, 200)

    def test_serializer_respone(self):
        respone = self.client.get('/polls/all_the_cars_from_the_active_company'.format(self.car.car_name))
        serializers_respone = views.priceList_for_company(respone,self.company.company_name)
        self.assertContains(serializers_respone, '120')
        self.assertContains(serializers_respone, 'HP')
        self.assertContains(serializers_respone, 'lada')


