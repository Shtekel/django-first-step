# coding=utf-8
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render_to_response
from .models import Car, Company, Prices
from StyleFrame import StyleFrame
from django.template import RequestContext





def all_the_cars_from_the_active_company(request):
    query = Car.objects.car_from_active_companys()
    data = serializers.serialize('json', query, use_natural_foreign_keys=True)
    return HttpResponse(data)


def Companies_that_active_and_their_name_contains_hebrew_yod(request):
    yod = ur'י'
    data = serializers.serialize('json',  Company.objects.active_companys().companys_that_contains_hebrew_letter(yod)
                                 , use_natural_foreign_keys=True)
    return HttpResponse(data)


def priceList_for_one_car(request, car_name):
    try:
        return HttpResponse(serializers.serialize('json', Prices.objects.prices_list_by_one_car(car_name),
                                                  use_natural_foreign_keys=True))
    except:
        print "fail"


def priceList_for_company(request, company_name):
    try:
        data = serializers.serialize('json', Prices.objects.prices_list_by_company(company_name),
                                     use_natural_foreign_keys=True)
        return HttpResponse(data)


    except:
        return HttpResponse(ur'{0} לא קיים כסוג מכונית'.format(company_name))


def min_price_list_in_every_company(request):
    data = serializers.serialize('json', Prices.objects.min_price_for_each_company()
                                 , use_natural_foreign_keys=True)
    return HttpResponse(data)






def export_to_xl(df, xl_name):
    df.head()
    input_styledFinalTable = StyleFrame(df)
    df.to_excel(xl_name, index=False)
    ew = StyleFrame.ExcelWriter(path=xl_name)
    input_styledFinalTable.to_excel(ew, row_to_add_filters=0, columns_and_rows_to_freeze='A2')
    ew.save()


def download_to_user(original_filename):
    my_file = open(original_filename, 'rb')
    response = HttpResponse(my_file, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="{0}.xlsx"'.format(original_filename.encode('utf-8'))
    return response


def index(request):
    return render_to_response('index.html', {}, context_instance=RequestContext(request))
