# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-05 10:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_name', models.CharField(max_length=200)),
                ('car_price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=200)),
                ('company_city', models.CharField(choices=[('\u05ea\u05dc \u05d0\u05d1\u05d9\u05d1', '\u05ea\u05dc \u05d0\u05d1\u05d9\u05d1'), ('\u05d9\u05d4\u05d5\u05d3', '\u05d9\u05d4\u05d5\u05d3'), ('\u05d7\u05d9\u05e4\u05d4', '\u05d7\u05d9\u05e4\u05d4')], default='\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05dd', max_length=200)),
                ('company_phone_number', models.CharField(max_length=200)),
                ('company_is_active', models.BooleanField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Prices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price_at_company', models.FloatField()),
                ('car_connection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Car')),
                ('company_connection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Company')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='Company',
            field=models.ManyToManyField(through='polls.Prices', to='polls.Company'),
        ),
    ]
